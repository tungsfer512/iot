import numpy as np
import json
import datetime


class LinearRegression:
    def __init__(self, learning_rate=0.1, epochs=10000):
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.weights, self.bias = None, 0
        self.costs = []
        self.n = 0

    @staticmethod
    def calc_cost(loss):
        return np.mean((loss ** 2))

    def fit(self, X, Y):
        self.costs = []
        self.weights = np.random.rand(X.shape[1])
        self.bias = 0
        self.n = X.shape[0]
        for epoch in range(self.epochs):
            loss = (np.dot(X, self.weights) + self.bias) - Y
            weight_gradient = 2 * np.dot(X.T, loss) / self.n
            bias_gradient = 2 * np.mean(loss)
            self.weights -= self.learning_rate * weight_gradient
            self.bias -= self.learning_rate * bias_gradient
            cost = self.calc_cost(loss)
            self.costs.append(cost)
            if epoch % (self.epochs / 10) == 0:
                print(f"Epoch {epoch} Cost {cost}")

    def coef_(self):
        return np.asarray(self.weights)

    def summary(self):
        print("Weights:", self.weights)
        print("Bias:", self.bias)
        print("Costs:", self.costs)

    def predict(self, X):
        return np.dot(X, self.weights) + self.bias

    def save(self):
        res = {
            "weights": self.weights.tolist(),
            "bias": self.bias
        }
        res = json.dumps(res)
        date_time = datetime.datetime.now().strftime("%H_%M_%S_%d_%m_%Y")
        file = open(f"./{date_time}.json", "w")
        file.write(res)
        file.close()
